package de.tekup.loan.Repository;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import de.tekup.soap.models.whitetest.Exam;

import org.springframework.util.Assert;

@Component
public class ExamRepository {
	private static final Map<String, Exam> exam = new HashMap<>();
	
	@PostConstruct
	public void initData() {
		Exam exam1 =new Exam();
		exam1.setCode("0001");
		exam1.setName("Java");
		exam.put(exam1.getCode(), exam1);
		
		Exam exam2 =new Exam();
		exam2.setCode("0002");
		exam2.setName("Python");
		exam.put(exam2.getCode(), exam2);
		
		Exam exam3 =new Exam();
		exam3.setCode("0003");
		exam3.setName("JavaScript");
		exam.put(exam3.getCode(), exam3);
			
	}
	
	public  Exam findExam(String Code) {
		
		Assert.notNull(Code , "The exam name must not be null");
		return exam.get(Code) ;
	}

}
