package de.tekup.loan.endpoint;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import de.tekup.loan.Service.WhittesteService;
import de.tekup.soap.models.whitetest.StudentRequest;
import de.tekup.soap.models.whitetest.WhiteTestResponse;




@Endpoint
public class WhiteTestEndpoint {
	
	public static final  String  namespace = "http://www.tekup.de/soap/models/whitetest" ;
	

	@Autowired
	private WhittesteService service ;
	
	
	@PayloadRoot(namespace = namespace , localPart = "StudentRequest")
	@ResponsePayload
	public  WhiteTestResponse checkKloanEligebilty (@RequestPayload StudentRequest Request) throws DatatypeConfigurationException, ParseException {
	
	
		return  service.getloanstatus(Request);
	}




}



