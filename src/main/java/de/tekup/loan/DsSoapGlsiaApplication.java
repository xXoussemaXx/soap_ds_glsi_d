package de.tekup.loan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class DsSoapGlsiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsSoapGlsiaApplication.class, args);
		
	}

}
