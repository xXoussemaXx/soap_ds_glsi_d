package de.tekup.loan.Repository;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import de.tekup.soap.models.whitetest.Exam;
import de.tekup.soap.models.whitetest.Student;

@Component
public class StudentRepository {
	
	private static final Map<Integer, Student> student = new HashMap<>();
	
	

	@PostConstruct
	public void initData() {
		
		
		
		Student student1 = new Student();
		student1.setId(2);
		student1.setName("oussea");
		student1.setAddress("tunis");
		student.put(student1.getId(), student1);
		
		Student student2 = new Student();
		student2.setId(3);
		student2.setName("mouhamed");
		student2.setAddress("nabeul");
		student.put(student2.getId(), student2);
		
	}
	


public Student findStudent(int studentId) {
	Assert.notNull(studentId , "The Student id must not be null");
	return student.get(studentId) ;
	
	
}

}
