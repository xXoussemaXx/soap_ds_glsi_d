package de.tekup.loan.Service;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.tekup.loan.Repository.DateRespository;
import de.tekup.loan.Repository.ExamRepository;
import de.tekup.loan.Repository.StudentRepository;
import de.tekup.soap.models.whitetest.ObjectFactory;

import de.tekup.soap.models.whitetest.StudentRequest;
import de.tekup.soap.models.whitetest.WhiteTestResponse;

@Service
public class WhittesteService {
	@Autowired
	private ExamRepository rep1;
	
	@Autowired
	private StudentRepository rep2;
	

	

	

	
	public WhiteTestResponse getloanstatus(StudentRequest request   ) throws DatatypeConfigurationException, ParseException {
		
		WhiteTestResponse TestResponse  = new ObjectFactory().createWhiteTestResponse();
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = format.parse("2021-05-30 11:15:00");

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);

		XMLGregorianCalendar xmlGregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
	    
		
			TestResponse.setExam(rep1.findExam(request.getExamCode()));
			TestResponse.setStudent(rep2.findStudent(request.getStudentId()));
			TestResponse.setDate(xmlGregCal );
		
			
	
		
	
		return TestResponse;
		 

}

}